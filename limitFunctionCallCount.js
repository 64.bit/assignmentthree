function limitFunctionCallCount(cb, n) {

    return () => {
        //checking if cb is called n times or not
        if(n>=0) return null;

        --n;
        return cb();
    }
}

module.exports=limitFunctionCallCount;