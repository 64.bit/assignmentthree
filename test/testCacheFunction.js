const cacheFunction=require('../cacheFunction');

const testCacheFunction=()=>{
    const callBack=()=> 'Hello World!';

    const func=cacheFunction(callBack);

    const firstAttempt=func(1);

    const secondAttempt=func(2);

    const thirdAttempt=func(2);

    const forthAttempt=func(3);
    
    console.log(firstAttempt,secondAttempt,thirdAttempt,forthAttempt);
}
testCacheFunction();
