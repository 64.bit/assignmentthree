const limitFunctionCallCount=require('../limitFunctionCallCount');
const testLimitFunctionCallCount=()=>{
    const callBack=()=> 'Hello World!';
    
    const testFunc1=limitFunctionCallCount(callBack,2);

    const testFunc2=limitFunctionCallCount(callBack,0);

    const testFunc3=limitFunctionCallCount(callBack,-1);
    
    //for storing result of every testcase
    let res1=[],res2=[],res3=[];

    for(let i=0;i<=2;i++){
        res1.push(testFunc1());
        res2.push(testFunc2());
        res3.push(testFunc3());
    }
    console.log(res1,res2,res3);
}
testLimitFunctionCallCount();
