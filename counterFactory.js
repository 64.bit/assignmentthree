const counterFactory=()=>{
    //initially the counter will be
   let counter=0;

   return {
       //this will increment the counter and return it
       increment:function(){
           counter++;
           return counter;
       },
       //this will decrement the counter and return it
       decrement:function(){
           counter--;
           return counter;
       }
   }
}

module.exports=counterFactory;