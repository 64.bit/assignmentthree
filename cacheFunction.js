function cacheFunction(cb) {
// for storing previously passed arguments
    let cache = [];
    let flag = false;

    return function (arg) {
        // cheking for if function is called for same argument previously
        if (flag !== true) {

            //checking if argument is present in the cache or not
            if (cache.find(elem=>elem===arg)!==undefined) {

                flag = true;

                return cache;
            } else {

                cache.push(arg);
                
                cb();
            }
        }
    }
}
module.exports=cacheFunction;
